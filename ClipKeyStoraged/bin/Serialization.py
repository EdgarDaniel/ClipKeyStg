import io
import pickle
import os

class Sesions:

    def __init__(self, login,passw):
        self.log = login
        self.passw = passw
        return

class WRSer:# Simulate a file.

    sesions=[]
    filename = os.path.dirname(__file__)+"/sesions.data"


    def __init__(self):
        print("Reading file...")
        self._read(False)

    def remove(self,log):
        for i in range(0,len(self.sesions),1):
            if(self.sesions[i].log == log):
                break

        print("Removing index: "+str(i))
        del self.sesions[i]
        self._writeUpdte()

    def _writeUpdte(self):
        out_s = io.BytesIO()

        with open(self.filename, 'wb') as out_s:
            pickle.dump(self.sesions, out_s)

    def write(self,log,passw):
        out_s = io.BytesIO()

        with open(self.filename, 'wb') as out_s:
            print('WRITING : {} ({})'.format(log, passw))
            self.sesions.append(Sesions(log,passw))
            pickle.dump(self.sesions, out_s)

    def exists(self,log)->bool:
        for s in self.sesions:
            if s.log == log:
                return True
        return False

    def getLogins(self,key:bool)->[]:
        with open(self.filename, 'rb') as in_s:
            while True:
                try:
                    o = pickle.load(in_s)
                except EOFError:
                    break
                else:
                    logs=[]
                    for s in o:
                         logs.append(s.log if key else s.passw)
                    return logs


# Set up a read-able stream
    def search(self,log):
        return self._read(True,log)

    def _read(self,search,log2Sear=""):
        with open(self.filename, 'rb') as in_s:
            while True:
                try:
                    o = pickle.load(in_s)
                except EOFError:
                    break
                else:
                    self.sesions = o

                    for sesion in self.sesions:
                        if search:
                            if sesion.log == log2Sear:
                                return sesion.passw
                        else:
                            print('READING : {} ({})'.format(sesion.log, sesion.passw))
