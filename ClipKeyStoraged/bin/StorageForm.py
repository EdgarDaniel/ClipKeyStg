from Serialization import *
serialz = WRSer()

if __name__ == '__main__':

    from tkinter import messagebox, Tk, Label, StringVar, Entry, Button, TclError as tcl
    from tkinter.ttk import Combobox
    from tkinter.simpledialog import askstring


    def add():
        name = askstring('Adding new session', 'Type your new login id: ')
        if name == "" or name is None:
            messagebox._show(title="Aborted", message="Adding session aborted")
        else:
            password = askstring('Adding new session', 'Type your new login password: ')

            if password == "" or password is None:
                messagebox._show(title="Aborted", message="Adding session aborted")
            else:
                save(name, password)


    def save(l, p):
        if serialz.exists(l):
            messagebox.showerror(title="Error", message="The log '{}' already exists".format(l))
        else:
            serialz.write(l, p)
            comboLogins["values"] = (*comboLogins["values"], l)
            comboKeys["values"] = (*comboKeys["values"], p)
            messagebox._show(title="Saved", message="A new values have been added")
            comboKeys.current(0)
            comboLogins.current(0)


    def delete():
        index = comboLogins.current()
        if index != -1:
            resp = messagebox.askquestion(title="Confirm",
                                          message="Are you sure you want to delete log: '{}'".format(login.get()))
            if resp == "yes":
                serialz.remove(login.get())
                comboLogins["values"] = serialz.getLogins(True)
                comboKeys["values"] = serialz.getLogins(False)
                try:
                    comboLogins.current(0)
                    comboKeys.current(0)
                except tcl:
                    pass

        else:
            messagebox.showerror(title="Error", message="You have not selected an item to delete")


    def changeComboLog(event):
        comboKeys.current(comboLogins.current())


    def changeComboKey(event):
        comboLogins.current(comboKeys.current())
        pass

    tk = Tk()
    tk.geometry("740x230+400-250")
    tk.title("Storage Logins")
    tk.config(bg="#2F343F")

    login = StringVar()
    key = StringVar()

    lbl = Label(text="Login Identifier: ", bg="#2F343F", font=("Ani", 18)).place(x=80, y=10)
    comboLogins = Combobox(tk, textvariable=login, font=("Ani", 15))
    comboLogins.bind("<<ComboboxSelected>>", changeComboLog)
    comboLogins.place(x=350, y=10)
    comboLogins["values"] = serialz.getLogins(True)

    lbl2 = Label(text="Password: ", bg="#2F343F", font=("Ani", 18)).place(x=100, y=100)
    comboKeys = Combobox(tk, textvariable=key, font=("Ani", 15))
    comboKeys.place(x=350, y=110)
    comboKeys.bind("<<ComboboxSelected>>", changeComboKey)
    comboKeys["values"] = serialz.getLogins(False)
    # show="*"

    btnSave = Button(tk, text='Add', bg="white", width="7", font=("Abyssinica SIL", 22), command=add)
    btnSave.place(x=170, y=170)

    btnDel = Button(tk, text='Delete', bg="white", width="7", font=("Abyssinica SIL", 22), command=delete)
    btnDel.place(x=400, y=170)

    for widget in tk.winfo_children():
        # check whether widget is instance of Label
        if isinstance(widget, Entry):
            widget.config(background="white")

        if isinstance(widget, Label):
            widget.config(foreground="white")

        else:
            widget.config(foreground="black")

    tk.mainloop()
