"""
A simple example of hooking the keyboard on Linux using pyxhook

Any key pressed prints out the keys values, program terminates when spacebar
is pressed.
"""
from __future__ import print_function
import StorageForm
import pyperclip

# Libraries we need
import pyxhook
import time

# This function is called every time a key is presssedgit
string=""

def kbevent(event, params):
    # print key info
    global string
    print(event)
    # If the ascii value matches enter, terminate the while loop
    if event.Ascii == 13:
        params['running'] = False
        pasw=StorageForm.serialz.search(string)
        if pasw is None:
            print("No id log found")
        
        else:
            pyperclip.copy(pasw)
            print("Clipboard updated")

    elif event.Ascii == 227:
        string=""
        print("Canceled content text")
    else:
        string+=chr(event.Ascii)


parameters={'running':True}
# Create hookmanager
hookman = pyxhook.HookManager(parameters=True)
# Define our callback to fire when a key is pressed down
hookman.KeyDown = kbevent
# Define our parameters for callback function
hookman.KeyDownParameters = parameters
# Start our listener
hookman.start()

# Create a loop to keep the application running
while parameters['running']:
    time.sleep(0.1)
# Close the listener when we are done
hookman.cancel()